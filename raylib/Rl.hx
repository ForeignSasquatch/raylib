package raylib;

@:buildXml("<include name='${haxelib:raylib}/Build.xml'/>")
@:include("raylib.h")
extern class Rl {
	//------------------------------------------------------------------------------------
	// Window and Graphics Device Functions (Module: core)
	//------------------------------------------------------------------------------------
	// Window-related functions

	/**
		Initialize window and OpenGL context
		@param width The window/opengl context width
		@param height The window/opengl context height
		@param title The window title
	**/
	@:native("InitWindow") static function initWindow(width:Int, height:Int, title:ConstCharStar):Void;

	/**
		Check if KEY_ESCAPE pressed or Close icon pressed
	**/
	@:native("WindowShouldClose") static function windowShouldClose():Bool;

	/**
		Close window and unload OpenGL context
	**/
	@:native("CloseWindow") static function closeWindow():Bool;

	/**
		Check if window has been initialized successfully
	**/
	@:native("IsWindowReady") static function isWindowReady():Bool;

	/**
		Check if window is currently fullscreen
	**/
	@:native("IsWindowFullscreen") static function isWindowFullscreen():Bool;

	/**
		Check if window is currently hidden
	**/
	@:native("IsWindowHidden") static function isWindowHidden():Bool;

	/**
		Check if window is currently minimized
	**/
	@:native("IsWindowMinimized") static function isWindowsMinimized():Bool;

	/**
		Check if window is currently maximized
	**/
	@:native("IsWindowMaximized") static function isWindowMaximized():Bool;

	/**
		Check if window is currently focused
	**/
	@:native("IsWindowFocused") static function isWindowFocused():Bool;

	/**
		Check if window has been resized last frame
	**/
	@:native("IsWindowResized") static function isWindowResized():Bool;

	/**
		Check if one specific window flag is enabled
		@param flag Window flag
	**/
	@:native("IsWindowState") static function isWindowState(flag:UInt):Bool;

	/**
		Set window configuration state using flags
		@param flag Window flag
	**/
	@:native("SetWindowState") static function setWindowState(flag:UInt):Void;

	/**
		Clear window configuration state flags
		@param flag Window flag
	**/
	@:native("ClearWindowState") static function clearWindowState(flag:UInt):Void;

	/**
		Toggle window state: fullscreen/windowed
	**/
	@:native("ToggleFullscreen") static function toggleFullscreen():Void;

	/**
		Set window state: maximized, if resizable
	**/
	@:native("MaximizeWindow") static function maximizeWindow():Void;

	/**
		Set window state: minimized, if resizable
	**/
	@:native("MinimizeWindow") static function minimizeWindow():Void;

	/**
		Set window state: not minimized/maximized
	**/
	@:native("RestoreWindow") static function restoreWindow():Void;

	/**
		Set icon for window
		@param image Icon image
	**/
	@:native("SetWindowIcon") static function setWindowIcon(image:Image):Void;

	/**
		Set title for window
		@param title Window title
	**/
	@:native("SetWindowTitle") static function setWindowTitle(title:ConstCharStar):Void;

	/**
		Set window position on screen
		@param x X coordinate on screen
		@param y Y coordinate on screen
	**/
	@:native("SetWindowPosition") static function setWindowPosition(x:Int, y:Int):Void;

	/**
		Set monitor for the current window
		@param monitor Monitor number to display
	**/
	@:native("SetWindowMonitor") static function setWindowMonitor(monitor:Int):Void;

	/**
		Set window minimum dimensions
		@param width Minimum window width
		@param height Minimum window height
	**/
	@:native("SetWindowMinSize") static function setWindowMinSize(width:Int, height:Int):Void;

	/**
		Set window dimensions
		@param width Window width
		@param height Window height
	**/
	@:native("SetWindowSize") static function setWindowSize(width:Int, height:Int):Void;

	/**
		Set window opacity
		@param opacity Opacity value from 0.0 to 1.0
	**/
	@:native("SetWindowOpacity") static function setWindowOpacity(opacity:Single):Void;

	/**
		Get current screen width
	**/
	@:native("GetScreenWidth") static function getScreenWidth():Int;

	/**
		Get current screen height
	**/
	@:native("GetScreenHeight") static function getScreenHeight():Int;

	/**
		Get current render width (it considers HiDPI)
	**/
	@:native("GetRenderWidth") static function getRenderWidth():Int;

	/**
		Get current render height (it considers HiDPI)
	**/
	@:native("GetRenderHeight") static function getRenderHeight():Int;

	/**
		Get number of connected monitors
	**/
	@:native("GetMonitorCount") static function getMonitorCount():Int;

	/**
		Get current connected monitor
	**/
	@:native("GetCurrentMonitor") static function getCurrentMonitor():Int;

	@:native("GetMonitorPosition") private static function _getMonitorPosition(monitor:Int):RlV2;

	/**
		Get specified monitor position
	**/
	public static inline function getMonitorPosition(monitor:Int):Vector2 {
		var p = _getMonitorPosition(monitor);
		return new Vector2(p.x, p.y);
	}

	/**
		Get specified monitor width (current video mode used by monitor)
		@param monitor Monitor to get the width from
	**/
	@:native("GetMonitorWidth") static function getMonitorWidth(monitor:Int):Int;

	/**
		Get specified monitor height (current video mode used by monitor)
		@param monitor Monitor to get the height from
	**/
	@:native("GetMonitorHeight") static function getMonitorHeight(monitor:Int):Int;

	/**
		Get specified monitor physical width in millimetres
		@param monitor Monitor to get the width from
	**/
	@:native("GetMonitorPhysicalWidth") static function getMonitorPhysicalWidth(monitor:Int):Int;

	/**
		Get specified monitor physical height in millimeteres
		@param monitor Monitor to get the height from
	**/
	@:native("GetMonitorPhysicalHeight") static function getMonitorPhysicalHeight(monitor:Int):Int;

	/**
		Get specified monitor refresh rate
		@param monitor Monitor to get the refresh rate from
	**/
	@:native("GetMonitorRefershRate") static function getMonitorRefershRate(monitor:Int):Int;

	@:native("GetWindowPosition") private static function _getWindowPosition():RlV2;

	/**
		Get window position XY on monitor
	**/
	public static inline function getWindowPosition():Vector2 {
		var p = _getWindowPosition();
		return new Vector2(p.x, p.y);
	}

	@:native("GetWindowScaleDPI") private static function _getWindowScaleDPI():RlV2;

	/**
		Get window scale DPI factor
	**/
	public static inline function getWindowScaleDPI():Vector2 {
		var p = _getWindowScaleDPI();
		return new Vector2(p.x, p.y);
	}

	/**
		Get the human-readable, UTF-8 encoded name of the primary monitor
		@param monitor Monitor to get the name of
	**/
	@:native("GetMonitorName") static function getMonitorName(monitor:Int):cpp.ConstCharStar;

	/**
		Set clipboard text content
		@param text Text to set copy to clipboard
	**/
	@:native("SetClipboardText") static function setClipboardText(text:cpp.ConstCharStar):Void;

	/**
		Get clipboard text content
	**/
	@:native("GetClipboardText") static function getClipboardText():cpp.ConstCharStar;

	/**
		Enable waiting for events on endDrawing(), no automatic event polling
	**/
	@:native("EnableEventWaiting") static function enableEventWaiting():Void;

	/**
		Disable waiting for events on endDrawing(),  automatic event polling
	**/
	@:native("DisableEventWaiting") static function disableEventWaiting():Void;
}
