package raylib;

/**
	Color, 4 components, R8G8B8A8 (32bit)
**/
class Color {
	/**
		Color red value
	**/
	public var r:Int;

	/**
		Color green value
	**/
	public var g:Int;

	/**
		Color blue value
	**/
	public var b:Int;

	/**
		Color alpha value
	**/
	public var a:Int;

	/**
		Creates new `Color`
		@param r Color red value
		@param g Color green value
		@param b Color blue value
		@param a Color alpha value
	**/
	public function new(r:Int, g:Int, b:Int, a:Int) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	private function torl_t():RlColor {
		RlColor.make(r, g, b, a);
	}
}

@:include("raylib.h")
@:native("Color")
@:structAccess
private extern class RlColor {
	var r:cpp.UInt8;
	var g:cpp.UInt8;
	var b:cpp.UInt8;
	var a:cpp.UInt8;

	static inline function make(r:cpp.UInt8, b:cpp.UInt8, g:cpp.UInt8, a:cpp.UInt8):RlColor {
		return untyped __cpp__("(Color){ (unsigned char){0}, (unsigned char){1}, (unsigned char){2}, (unsigned char){3} }", r, g, b, a);
	}
}
