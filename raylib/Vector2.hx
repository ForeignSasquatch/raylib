package raylib;

/**
	Vector2, 2 components
**/
class Vector2 {
	/**
		Vector x component
	**/
	public var x:Float;

	/**
		Vector y component
	**/
	public var y:Float;

	/**
		Creates new `Vector2`
		@param x Vector x component
		@param y Vector y component
	**/
	public function new(x:Float, y:Float) {
		this.x = x;
		this.y = y;
	}

	private function torl_t():RlV2 {
		return RlV2.make(this.x, this.y);
	}
}

@:allow(raylib.Rl)
@:include("raylib.h")
@:include("Vector2")
@:structAccess
private extern class RlV2 {
	var x:cpp.Float32;
	var y:cpp.Float32;

	static inline function make(x:cpp.Float32, y:cpp.Float32):RlV2 {
		return untyped __cpp__("(Vector2){ (Float){0}, (Float){1} }", x, y);
	}
}
